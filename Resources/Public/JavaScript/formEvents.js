/*
 * Javascript functions for TYPO3 extension sr_email_subscribe
 *
 */
(function () {
	window.addEventListener('load', (event) => {
		// Click event when cancelling delete
		var submit = document.getElementById("tx_sremailsubscribe_pi1_delete_doNotSave");
		if (submit !== null) {
			submit.addEventListener('click', (event) => {
				var form = submit.form;
				if (form) {
					var element = form.elements.namedItem("tx_sremailsubscribe_pi1[cmd]");
					if (element) {
						element.setAttribute('value', 'edit');
					}
				}
				return true;
			});
		}
	});
})();