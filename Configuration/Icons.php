<?php
use TYPO3\CMS\Core\Imaging\IconProvider\BitmapIconProvider;
return [
   // Icon identifier
   'ce-sjbr-email-subscribe' => [
	   // Icon provider class
	   'provider' => BitmapIconProvider::class,
	   // The source SVG for the SvgIconProvider
	   'source' => 'EXT:sr_email_subscribe/Resources/Public/Icons/Extension.gif'
   ]
];