<?php
defined('TYPO3') or die();

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;

call_user_func(
    function($extKey)
    {
		$pluginSignature = 'sr_email_subscribe_pi1';
		 // Add the plugins to the list of plugins
		ExtensionManagementUtility::addPlugin(
			 [
				 'LLL:EXT:sr_email_subscribe/Resources/Private/Language/locallang_db.xlf:tt_content.email_subscribe',
				 $pluginSignature,
				 'ce-sjbr-email-subscribe',
				 'forms'
			 ],
			 'CType',
			 $extKey
		 );
		ExtensionManagementUtility::addPiFlexFormValue('*', 'FILE:EXT:sr_email_subscribe/Configuration/FlexForms/flexform_ds_pi1.xml', $pluginSignature);
		// Activate the display of the FlexForm field
		ExtensionManagementUtility::addToAllTCAtypes(
			'tt_content',
			'--div--;LLL:EXT:core/Resources/Private/Language/Form/locallang_tabs.xlf:plugin, pi_flexform,pages',
			$pluginSignature,
			'after:palette:headers'
		);
	},
	'sr_email_subscribe'
);