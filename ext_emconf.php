<?php
/*
 * Extension Manager configuration file for ext "sr_email_subscribe".
 *
 */
$EM_CONF[$_EXTKEY] = [
	'title' => 'Email Address Subscription',
	'description' => 'Email address subscription for TYPO3 CMS.',
	'category' => 'plugin',
	'version' => '13.4.0',
	'state' => 'stable',
	'clearcacheonload' => 1,
	'author' => 'Stanislas Rolland',
	'author_email' => 'typo3AAAA@sjbr.ca',
	'author_company' => 'SJBR',
	'constraints' => [
		'depends' => [
			'typo3' => '13.4.0-13.4.99',
			'sr_feuser_register' => '13.4.0-13.4.99',
			'tt_address' => '9.0.0-9.0.99',
			'static_info_tables' => '13.4.0-13.4.99'
		],
		'suggests' => [
			'sr_freecap' => '13.4.0-13.4.99'
		]
    ],
    'autoload' => [
        'psr-4' => [
        	'SJBR\\SrEmailSubscribe\\' => 'Classes'
        ]
    ]
];